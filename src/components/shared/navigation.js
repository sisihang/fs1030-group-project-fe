import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container } from 'reactstrap';
import { NavLink as RouteLink } from 'react-router-dom';

import './navigation.css';
import admin from '../images/admin-icon.png';

const Navigation = () => {
    // const [isOpen, setIsOpen] = useState(false)
    // const toggle = () => setIsOpen(!isOpen)

    return (
        // <Navbar flex-direction="column" dark color="dark" >
        //     <Container>
        //     <NavbarBrand  defaultActiveKey="/"></NavbarBrand>
        //     {/* <NavbarToggler onClick={toggle} /> */}
        //     {/* <Collapse isOpen={isOpen} navbar> */}
        //         <Nav className="ml-auto" navbar>
        //             <NavItem>
        //                 <NavLink tag={RouteLink} to="/">Patient</NavLink>
        //             </NavItem>
        //             <NavItem>
        //                 <NavLink tag={RouteLink} to="/medHistory">Medical History</NavLink>
        //             </NavItem>
        //             <NavItem>
        //                 <NavLink tag={RouteLink} to="/meds">Medication</NavLink>
        //             </NavItem>
        //             <NavItem>
        //                <NavLink tag={RouteLink} to="/allergies">Allergies</NavLink>
        //             </NavItem>
        //             <NavItem>
        //                <NavLink tag={RouteLink} to="/immune">Immunizations</NavLink>
        //             </NavItem>
        //             <NavItem>
        //                <NavLink tag={RouteLink} to="/lab">Labratory Test &amp; Reults</NavLink>
        //             </NavItem>
        //             <NavItem>
        //                <NavLink tag={RouteLink} to="/radio">Radiology</NavLink>
        //             </NavItem>
        //             <NavItem>
        //                <NavLink tag={RouteLink} to="/billing">Billing</NavLink>
        //             </NavItem>
        //             <NavItem>
        //                <NavLink tag={RouteLink} to="/notes">Notes</NavLink>
        //             </NavItem>
        //             {/* <NavItem>
        //                 <NavLink tag={RouteLink} to="/submissions">Administration</NavLink>
        //             </NavItem> */}
        //         </Nav>
        //     {/* </Collapse> */}
        //     </Container>
        // </Navbar>
        
        <Nav fill variant="tabs" defaultactiveKey="/patient" color="light" className="my-2">
            <NavItem className="navTab" style={{backgroundColor:'orange'}}>
                <NavLink tag={RouteLink} to="/" style={{color: 'white'}}>Patient</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'aqua'}}>
                <NavLink tag={RouteLink} to="/medHistory" style={{color: 'white'}}>Medical History</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'#345095'}}>
                <NavLink tag={RouteLink} to="/meds" style={{color: 'white'}}>Medication</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'purple'}}>
                <NavLink tag={RouteLink} to="/allergies" style={{color: 'white'}}>Allergies</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'red'}}>
                <NavLink tag={RouteLink} to="/immune" style={{color: 'white'}}>Immunizations</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'blue'}}>
                <NavLink tag={RouteLink} to="/lab" style={{color: 'white'}}>Labratory Test &amp; Reults</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'green'}}>
                <NavLink tag={RouteLink} to="/radio" style={{color: 'white'}}>Radiology</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'#532014', color: 'white'}}>
                <NavLink tag={RouteLink} to="/billing" style={{color: 'white'}}>Billing</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'grey'}}>
                <NavLink tag={RouteLink} to="/notes" style={{color: 'white'}}>Notes</NavLink>
            </NavItem>
            <NavItem className="navTab" style={{backgroundColor:'pink'}}>
                <NavLink tag={RouteLink} to="/admin" > <img className="checkImg" src={admin} alt="" /></NavLink>
            </NavItem>
        </Nav>
       
    )
}

export default Navigation