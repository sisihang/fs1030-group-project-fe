import React from 'react'
import { Container } from 'reactstrap'

const Footer = () => {
    return(
    <footer  fixed="bottom" className="py-2 bg-dark">
        <Container>
            <p className="m-0 text-center text-white">Copyright &copy; 2021 GRP2</p>
        </Container>
    </footer>
  )
}

export default Footer