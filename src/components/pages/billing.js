import React from 'react';
import { Container, Row, Col, Button, CardBody, CardText, Card, FormGroup, Form, Label } from 'reactstrap';
import '../styles/billing.css';


const Billings = () => {
    
      
        // const formSubmit = async event => {
        //     event.preventDefault()
        //     const response = await fetch('http://localhost:4000/contact_form/entries', {
        //         method: 'POST',
        //         headers: {
        //             'Accept': 'application/json',
        //             'Content-Type': 'application/json'
        //           },
        //         body: JSON.stringify({name, email, phoneNumber, content})
        //     })
        //     const payload = await response.json()
        //     if (response.status >= 400) {
        //         alert(`Oops! Error: ${payload.message}`)
        //     } else {
        //         alert(`Congrats! Your form has been submitted.  We'll get back to you shortly! `)
        //     }
        //     setName("");
        //     setEmail("");
        //     setPhoneNumber("");
        //     setContent("");
        // }
    
        return (
            <Container className="bgcolorBilling px-3">
                {/* <Card className="text-white bg-secondary my-5 py-4 text-center">
                    <CardBody>
                        <CardText className="text-white m-0"><span className="text">Get in touch...</span> &nbsp;&nbsp;&nbsp;&nbsp; Fill out the form and I'll get back to you within 24 hours!</CardText>
                    </CardBody>
                </Card> */}
                 <Row style={{padding: '30px'}}>
                <h3>Billings: John Doe</h3>
            </Row>
                <Row className="my-5">
                <Col lg="8">
                <Form className="my-5" >
                    <FormGroup row>
                        <Label for="emailEntry" sm={2}>Email</Label>
                        <Col sm={10}>
                      
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="phoneEntry" sm={2}>Phone Number</Label>
                        <Col sm={10}>
                       
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="nameEntry" sm={2}>Full Name</Label>
                        <Col sm={10}>
                      
                        </Col>
                    </FormGroup>
    
                    <FormGroup row>
                        <Label for="messageEntry" sm={2}>Message</Label>
                        <Col sm={10}>
                       
                        </Col>
                    </FormGroup>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                        <Button color="success">Submit</Button>
                        </Col>
                    </FormGroup>
                </Form>
                </Col>
                <Col lg="4">
                
                </Col>
                </Row>
            </Container>
          )
        }
    

export default Billings