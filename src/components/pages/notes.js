import React from 'react';
import { Container, Row, Col, Button, CardBody, CardText, Card } from 'reactstrap';



const Notes = () => {
    return(
        <Container>
            <Row className="my-5">
                <Col sm="4">
                    <img className="img-fluid rounded mb-4 mb-lg-0" src="" alt="" />
                </Col>
                <Col lg="5">
                    <h3 className="font-weight-light">Notes</h3>
                    <br />
                    Any extra Notes about the patient can go here!<br /><br />

                </Col>
            </Row>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0">Welcome to the GR2 EMR Software System</span>
                    </CardText>
                </CardBody>
            </Card>

        </Container>
    )
}

export default Notes