import React from 'react';
import { Container, Row, Col, Button, CardBody, CardText, Card, Table } from 'reactstrap';
import '../styles/immune.css';


const Immune = () => {
    return (
        <Container className="bgcolor my-5">
            <Row style={{padding: '30px'}}>
                <h3>Immunizations: John Doe</h3>
            </Row>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Immunization</th>
                        <th>Recieved</th>
                        <th>Given By</th>
                        <th>Comments</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>2008/12/23</td>
                        <td>Diptheria</td>
                        <td>yes</td>
                        <td>Dr. Doug Ross</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2009/02/23</td>
                        <td>Tetanus</td>
                        <td>yes</td>
                        <td>Dr. Derek Shephard</td>
                        <td>Due again 2019</td>
                    </tr>
                    <tr>
                        <td>2017/01/21</td>
                        <td>Varicella</td>
                        <td>yes</td>
                        <td>Dr. Dougie Howser</td>
                        <td>slight rash reaction</td>
                    </tr>
                   
                </tbody>
            </Table>
            <Card className="text-white my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-5"></span>
                    </CardText>
                </CardBody>
            </Card>

        </Container>
    )
}

export default Immune