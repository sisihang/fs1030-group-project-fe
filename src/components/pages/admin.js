import React, { useState } from 'react';
import { Form, FormGroup, Col, Row, Input, Label, Button, Container, CardBody, Card, CardText } from 'reactstrap';
import { useHistory, useLocation } from 'react-router-dom'
import'../styles/admin.css';

const Admin = () => {
    // let history = useHistory();
    // let location = useLocation();
    // const [username, setUserName] = useState("")
    // const [password, setPassword] = useState("")
    // const [admin, setAdmin] = useState(true)
    
    
    

    // const formSubmit = async event => {
    //     event.preventDefault()
    //     const response = await fetch('http://localhost:4000/admin', {
    //         method: 'POST',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json'
    //           },
    //         body: JSON.stringify({username, password})
    //     })
    //     const payload = await response.json()
    //     if (response.status >= 400) {
    //         setAdmin(false)
    //     } else {
    //         console.log('your In');
    //         sessionStorage.setItem('token', payload.token)
    //         let { from } = location.state || { from: { pathname: "newUser" } };
    //         history.replace(from);
    //     }
            
    // }

    // const resetInputField = () => {
    //     Array.from(document.querySelectorAll("input")).forEach(
    //       input => (input.value = "")
    //     );
    //     setPassword("");
    //     setUserName("");
    //   };

    return (
            <Container>
                 {/* {!admin && 
                <Card className="text-white bg-primary my-5 py-4 text-center">
                <CardBody>
                    <CardText className="text-white m-0">Invalid credentials, please try again</CardText>
                </CardBody>
                </Card> */}
                <Card className="text-white bg-secondary my-4 py-0 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0">Welcome to the GR2 EMR Software System</span>
                    </CardText>
                </CardBody>
            </Card>
                <Card className="text-white bg-info my-5 py-3 text-center">
                <CardBody>
                    <CardText className="text-white m-0"><span className="text">Administration...</span> &nbsp;&nbsp;&nbsp;&nbsp; Please enter your username and password to access the system!</CardText>
                </CardBody>
                </Card>
            <Row className="my-5 locate">
            <Col lg="8">
            <Form className="my-2">
              <FormGroup row>
                <Label for="usernameEntry">Username:</Label>
                <Input type="text" name="username" id="usernameEntry" placeholder="Username"  />
              </FormGroup>
              <FormGroup row>
                <Label for="passwordEntry">Password:</Label>
                <Input type="password" name="password" id="passwordEntry" placeholder="Valid password" />
              </FormGroup>
              
             <Col sm={{ size: 10, offset: 4 }}>
                <Button as="input" type="button" value="submit" color="success">Submit</Button>{' '}
                <Button as="input" type="reset" value="Reset" color="success" >Reset</Button>
               
                
            </Col>
            
    
        </Form>
        </Col>
           </Row>
        </Container>
      )
    }

    export default Admin
   