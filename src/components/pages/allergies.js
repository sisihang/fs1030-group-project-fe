import React from 'react';
import { Container, Row, Col, Button, CardBody, CardText, Card } from 'reactstrap';
import '../styles/allergies.css';


const Allergies = () => {
    return(
        <Container className="bgcolorAllergy my-5">
            <Row className="my-5">
                <Col sm="4">
                    <img className="img-fluid rounded mb-4 mb-lg-0" src="" alt="" />
                </Col>
                <Col lg="5">
                    <h3 className="font-weight-light">Allergies</h3>
                    <br />
                    Patient's allergy information goes here!<br /><br />
                   
                </Col>
            </Row>
           
            
        </Container>
    )
}

export default Allergies