import React from 'react';
import { Container, Row, Col, Button, CardBody, CardText, Card } from 'reactstrap';
import '../styles/radio.css';


const Radio = () => {
    return(
        <Container className="backcolor">
            <Row className="my-5 mx-0">
                <Col sm="2">
                <h3 className="font-weight-light">Radiology</h3>
                    <br />
                    Patient's radiology info and imaging goes here!<br /><br />
                </Col>
                <Col sm="5">
                    <h3 className="font-weight-light">Radiology</h3>
                    <br />
                    Patient's radiology info and imaging goes here!<br /><br />
                   
                </Col>
                <Col sm="5">
                    <h3 className="font-weight-light">Radiology</h3>
                    <br />
                    Patient's radiology info and imaging goes here!<br /><br />
                   
                </Col>
            </Row>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0">Welcome to the GR2 EMR Software System</span>
                    </CardText>
                </CardBody>
            </Card>
            
        </Container>
    )
}

export default Radio