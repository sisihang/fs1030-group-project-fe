import React from 'react';
import { Container, Row, Col, Button, CardBody, CardText, Card } from 'reactstrap';



const Lab = () => {
    return(
        <Container>
            <Row className="my-5">
                <Col sm="4">
                    
                </Col>
                <Col lg="5">
                    <h3 className="font-weight-light">LabratoryTest &amp; Results</h3>
                    <br />
                    Patient's lab work and results goes here!<br /><br />
                   
                </Col>
            </Row>
            <Card className="text-white bg-secondary my-5 py-4 text-center">
                <CardBody>
                    <CardText>
                        <span className="text-white m-0"></span>
                    </CardText>
                </CardBody>
            </Card>
            
        </Container>
    )
}

export default Lab