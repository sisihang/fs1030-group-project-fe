import React from 'react'
import './App.css';
import Navigation  from './components/shared/navigation'
import Footer from './components/shared/footer'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Patients from './components/pages/patient'
import MedHistory from './components/pages/medHistory'
import Meds from './components/pages/meds'
import Allergies from './components/pages/allergies'
import Immune from './components/pages/immune'
import Lab from './components/pages/lab'
import Radio from './components/pages/radio'
import Billing from './components/pages/billing'
import Notes from './components/pages/notes'
import Admin from './components/pages/admin'

// import PrivateRoute from './components/shared/PrivateRoute'

function App() {
  return (
    <BrowserRouter>
        <Navigation />        
        <Routes>
          <Route exact path="/" element={<Patients />} />
          <Route exact path="/medHistory" element={<MedHistory />} />
          <Route exact path="/meds" element={<Meds />} />
          <Route exact path="/allergies" element={<Allergies />} />
          <Route exact path="/immune" element={<Immune />} />
          <Route exact path="/lab" element={<Lab />} />
          <Route exact path="/radio" element={<Radio />} />
          <Route exact path="/billing" element={<Billing />} />
          <Route exact path="/notes" element={<Notes />} />
          <Route exact path="/admin" element={<Admin />} />
         {/*} <PrivateRoute path="/submissions">
            <Listing /> 
  </PrivateRoute> */}
        </Routes>
        <Footer />  
    </BrowserRouter>
  );
}

export default App;
